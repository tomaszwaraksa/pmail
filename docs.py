import pypandoc

# Create RST documentation files from MD
with open('README.md', 'r') as readme:
    md = readme.read()
    rst = pypandoc.convert(md, 'rst', format='markdown')
    with open('README.rst', 'w') as outfile:
        outfile.write(rst)
