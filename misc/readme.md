# Local installation 
1. Navigate to the folder where application has been unpacked.
2. Run `pip install . -e` to install `pmail` as globally available command
3. Run `pip install . -e --upgrade` to upgrade the existing installation.

# Installation into PyPI
## Prerequisites
The following build and packaging tools are required:`twine`,  `sdist` and `wheel`. 
Install them using `pip`:

    pip install twine
    pip install sdist
    pip install wheel 

Build redistributable package:

    python setup.py sdist
    
Build wheel (precompiled package):

    python setup.py bdist_wheel
    
Upload to PyPI:

    twine upload dist/*
    

# Generating readme.rst
Use `pypandoc` tool. On `macOs`, first install the following requirements:

    brew install pandoc
    pip install pypandoc
    
Then use the following Python code to create a script converting `README.md` file to `README.rst`:

    import pypandoc
    
    with open('README.md', 'r') as readme:
        md = readme.read()
        rst = pypandoc.convert(md, 'rst', format='markdown')
        with open('README.rst', 'w') as outfile:
            outfile.write(rst)
